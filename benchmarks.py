import numpy as np
from matplotlib import pyplot as plt

def ode_model_pressure_benchmark(P, t, ap, bp, cp, P0, q, dq):
    ''' Return the derivative dP/dt at time, t, for given parameters.

        Parameters:
        -----------
        P : float
            Dependent variable.
        t : float
            Independent variable.
        ap : float
            Extraction rate strength parameter.
        bp : float
            Recharge strength parameter. 
        cp : float
            Slow drainage strength parameter. 
        P0 : float
            Ambient value of dependent variable.
        q  : float
            Extraction rate. Assumed to be constant.
        dq : float
            Rate of change in extraction rate. Assumed to be constant.

        Returns:
        --------
        dPdt : float
            Derivative of dependent variable with respect to independent variable.

    '''
    return -ap * q - bp * (P - P0) - cp * dq


def solve_ode_pressure_benchmark(t, ap, bp, cp, P0, q, dq, Pt0):
    ''' Solve an ODE numerically.

        Parameters:
        -----------
        t : array-like
            Times at which to output solution.
        ap : float
            Extraction rate strength parameter.
        bp : float
            Recharge strength parameter. 
        cp : float
            Slow drainage strength parameter. 
        P0 : float
            Ambient value of dependent variable.
        q  : float
            Extraction rate. Assumed to be constant.
        dq : float
            Rate of change in extraction rate. Assumed to be constant.
        Pt0: float
            Initial pressure at initial time.

        Returns:
        --------
        P : array-like
            Pressure at times t.

        Notes:
        ------
        Format of code taken from lumped_parameter_model.py
    '''

    Pm = [Pt0]                            # initial value
    for t0,t1 in zip(t[:-1],t[1:]):           # solve at pressure steps
        dPdt1 = ode_model_pressure_benchmark(Pm[-1], t0, ap, bp, cp, P0, q, dq)   # predictor gradient
        Pp = Pm[-1] + dPdt1*(t1-t0)             # predictor step
        dPdt2 = ode_model_pressure_benchmark(Pp, t1, ap, bp, cp, P0, q, dq)       # corrector gradient
        Pm.append(Pm[-1] + 0.5*(t1-t0)*(dPdt2+dPdt1))  # corrector step
    return Pm


def pressure_benchmark(save=False):
    ''' Plots benchmark for pressure ODE.

        Parameters:
        -----------
        save : boolean
            Whether to save the image as a png or not.
            Default False.
        
        Notes:
        ------
        Performs benchmark analysis, error analysis, and timestep convergence.
        Parameters were randomly selected.
    '''
    # randomly selected parameters
    ap = 2.
    bp = 1.
    cp = 0.
    q = 4.
    dq = 0.
    P0 = 10.
    
    # set initial pressure and time range
    Pt0 = 1.
    t0 = 0.
    t1 = 10.

    # calculate constant for analytical solution
    c1 = (Pt0 + ap * q / bp - P0) / np.exp(-bp * t0)

    # create range of values for h from 10^1 to 10^-1, and initialise arrays
    h = np.logspace(0.25, -0.5)
    model_times = []
    final_model_pressures = []
    final_analytical_pressures = []
    
    # calculate values for timestep convergence analysis
    for step_size in h:
        model_times.append(np.arange(t0, t1, step_size))
        final_model_pressures.append(solve_ode_pressure_benchmark(model_times[-1], ap, bp, cp, P0, q, dq, Pt0)[-1])
        final_analytical_pressures.append(ap * q / bp + P0 + c1 * np.exp(-bp * model_times[-1])[-1])

    # calculate values for benchmark and error analysis
    good_h = 0.5
    bad_h = 1.5
    t_good_model = np.arange(t0, t1, good_h)
    t_bad_model = np.arange(t0, t1, bad_h)
    P_good_model = solve_ode_pressure_benchmark(t_good_model, ap, bp, cp, P0, q, dq, Pt0)
    P_bad_model = solve_ode_pressure_benchmark(t_bad_model, ap, bp, cp, P0, q, dq, Pt0)
    P_analytical = -ap * q / bp + P0 + c1 * np.exp(-bp * t_good_model)
    P_error = []
    for i in range(len(t_good_model)):
        P_error.append(abs(P_good_model[i] - P_analytical[i]))
        P_good_model[i]
        P_analytical[i]

    # plot
    f, ax = plt.subplots(ncols=3)
    f.set_size_inches(12, 9)

    # benchmark
    ax[0].hlines(P0 - ap * q, t0, t1, colors='g', linestyles='dashed', label='steady-state solution')
    ax[0].plot(t_good_model, P_good_model, 'b*', label='model (step size = {:.1f})'.format(good_h))
    ax[0].plot(t_bad_model, P_bad_model, 'r*', label='model (step size = {:.1f})'.format(bad_h))
    ax[0].plot(t_good_model, P_analytical, 'g-', label='analytical solution')
    ax[0].legend()
    ax[0].set_title('Benchmark of Pressure ODE\nap = {:.1f}, bp = {:.1f}, cp = {:.1f}, q = {:.1f}'.format(ap, bp, cp, q))
    ax[0].set_xlabel('t')
    ax[0].set_ylabel('P(t) x 10\u2074')

    # error analysis
    ax[1].plot(t_good_model, P_error, 'r--')
    ax[1].set_title('Error Analysis')
    ax[1].set_xlabel('t')
    ax[1].set_ylabel('model error against benchmark')

    # timestep convergence analysis
    ax[2].plot(1 / h, final_model_pressures, 'ko', markersize=4)
    ax[2].set_title('Timestep Convergence Analysis')
    ax[2].set_xlabel('1 / step size')
    ax[2].set_ylabel('P(t = {:.1f}) x 10\u2074'.format(t1))
    ax[2].yaxis.set_label_position("right")
    ax[2].yaxis.tick_right()

    # save or show
    if save:
        plt.savefig('pressure_benchmark', bbox_inches='tight')
    else:
        plt.show()


def pressure_gold_secret(save=False):
    ''' Plots David's Gold Secret for pressure ODE.

        Parameters:
        -----------
        save : boolean
            Whether to save the image as a png or not.
            Default False.
        
        Notes:
        ------
        Changes parameter values to extremes, then runs models.
    '''
    # sample parameters taken from calibration
    ap = 1
    bp = 1
    cp = 1
    q = 1
    dq = 1
    P0 = 10
    
    # set initial pressure and time range
    Pt0 = 1
    t0 = 0
    t1 = 10

    h = 0.5

    t_model = np.arange(t0, t1, h)

    f, ax = plt.subplots(2, 2)
    
    # setting aq to be very large
    ap_model = solve_ode_pressure_benchmark(t_model, ap * 100, bp, cp, P0, q, dq, Pt0)
    ax[0, 0].plot(t_model, ap_model, 'r--', label = 'ap >> bp, cp, q')
    ax[0, 0].legend()
    ax[0, 0].set_title('Pressure Model Solutions\nat Extreme Values')
    ax[0, 0].set_ylabel('P(t)')

    # setting all except bp to equal 0
    bp_model = solve_ode_pressure_benchmark(t_model, ap * 0, bp, cp * 0, P0, q * 0, dq, Pt0)
    ax[0, 1].plot(t_model, bp_model, 'b--', label = 'ap, cp, q = 0')
    ax[0, 1].legend()

    # setting cp to be very large
    cp_model = solve_ode_pressure_benchmark(t_model, ap, bp, cp * 100, P0, q, dq, Pt0)
    ax[1, 1].plot(t_model, cp_model, 'g--', label = 'cp >> ap, bp, q')
    ax[1, 1].legend()
    ax[1, 1].set_xlabel('t')

    # setting q to be very large
    q_model = solve_ode_pressure_benchmark(t_model, ap, bp, cp, P0, q * 100, dq, Pt0)
    ax[1, 0].plot(t_model, q_model, 'y--', label = 'q >> ap, bp, cp')
    ax[1, 0].legend()
    ax[1, 0].set_xlabel('t')
    ax[1, 0].set_ylabel('P(t)')

    if save:
        plt.savefig('pressure_gold_secret')
    else:
        plt.show()
