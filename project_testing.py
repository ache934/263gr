from project import *
from numpy.linalg import norm
tol = 1.e-6

tq = [1,2,3]
tP = [1,2,3]
P = [100,100,100]
q = [30,30,30]
dqdt = [1,1,1]
response1 = solve_ode_pressure(t = tP, ap = 2, bp = 3, cp = 4, P0 = 400)
#this isnt complete at all, doing the calcs by hand

#functions from an earlier lab, using for reference
def test_solve_ode():
	"""
	Test if function solve_ode is working properly by comparing it with a known result.

	Remember to consider any edge cases that may be relevant.
	"""

	#general case for q = 2
	ts, xs = solve_ode(f=ode_model, t0=0, t1=2, dt=1, x0=0, pars=[3,4,5]) #pars = [a,b,x0] (x0 is ambient dependent value)
	x_soln = [0,-26,-156]
	assert norm(xs - x_soln) < tol         #using precalculated data

	# #initial and ambient identical for q = 3
	ts,xs = solve_ode(f=ode_model, t0=0, t1=2, dt=1, x0=0, pars=[-2,-4,0])
	x_soln = [0,-18,-252]
	assert norm(xs - x_soln) < tol         #using precalculated data

	pass

def test_ode_model():
	"""
	Test if function ode_model is working properly by comparing it with a known result.

	Remember to consider any edge cases that may be relevant.
	"""
	#general case
	response1 = ode_model(t=0, x=1, q=2, a=3, b=4, x0=5)
	assert(response1 == 22)
	#first term zero
	response2 = ode_model(t=0, x=1, q=0, a=3, b=4, x0=5)
	assert(response2 == 16)
	#second term zero
	response3 = ode_model(t=0, x=1, q=2, a=3, b=0, x0=5)
	assert(response3 == 6)

	pass

print("success")