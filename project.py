from matplotlib import pyplot as plt
from matplotlib.legend import Legend
import numpy as np
from scipy.optimize import curve_fit
from benchmarks import *

### Data generation ###

# Pressure
tP_data, w_data = np.genfromtxt('gr_p.txt',delimiter=',',skip_header=1).T
P_data = (101325 + 1000 * 9.81 * w_data / 2)

# Temperature
tT_data, T_data = np.genfromtxt('gr_T.txt',delimiter=',',skip_header=1).T

# Extraction rate data
qt_data_total, q_data_total = np.genfromtxt('gr_q1.txt',delimiter=',',skip_header=1).T
qt_data_rhy, q_data_rhy = np.genfromtxt('gr_q2.txt',delimiter=',',skip_header=1).T

# Generate extraction rate data for non-rhyolite areas
qt_data = np.linspace(1950, 2015, 651)
q_data = np.interp(qt_data, qt_data_total, q_data_total) - np.interp(qt_data, qt_data_rhy, q_data_rhy)

# Extraction rate derivative data
dq_data = 0 * q_data
dq_data[1:-1] = (q_data[2:] - q_data[:-2]) / (qt_data[2:] - qt_data[:-2])       # central differences
dq_data[0] = (q_data[1] - q_data[0]) / (qt_data[1] - qt_data[0])                # forward difference
dq_data[-1] = (q_data[-1] - q_data[-2])/(qt_data[-1] - qt_data[-2])             # backward difference

def pressure_ode(t, P, ap, bp, cp, P0):
    ''' Return the derivative dP/dt at time, t, for given parameters.

        Parameters:
        -----------
        t : float
            Time.
        P : float
            Pressure at time t.
        ap : float
            Extraction rate strength parameter.
        bp : float
            Recharge strength parameter. 
        cp : float
            Slow drainage strength parameter. 
        P0 : float
            Ambient/intial value of pressure.

        Returns:
        --------
        dPdt : float
            Rate of change of pressure at given time.
    '''
    
    # Interpolate value of extraction rate/derivative at given time
    qt = np.interp(t, qt_data, q_data)
    dqdt = np.interp(t, qt_data, dq_data)

    return -ap * qt - bp * (P - P0) - cp * dqdt

def temperature_ode(t, T, aT, bT, Tc, T0):
    ''' Return the derivative dT/dt at time, t, for given parameters.

        Parameters:
        -----------
        t : float
            Time.
        T : float
            Temperature at given time.
        aT : float
            Cold water inflow strength parameter.
        bT : float
            Conduction strength parameter. 
        Tc : float
            Temperature of cold water.
        T0 : float
            Ambient/initial value of temperature.

        Returns:
        --------
        dTdt : float
            Derivative of dependent variable with respect to independent variable.

    '''
    
    # Interpolate pressure at given time
    Pt = np.interp(t, tP_model, P_model)

    return -aT * (bp / ap) * (Pt - P0) * (Tc - T) - bT * (T - T0)

def improved_euler_step(f, t, Y, h, pars=[]):
    ''' Return value of dependent variable Y at time (t + h),
        using improved Euler calculation.

        Parameters:
        -----------
        f : callable
            Derivative function.
        t : float
            Current value of independent variable.
        Y : float
            Current value of dependent variable.
        h : float
            Step size.
        pars : array
            Additonal parameters for derivative function, if any.

        Returns:
        --------
        Y1 : float
            value of dependent variable Y at time (t + h).
    '''

    # Current gradient
    dY0 = f(t, Y, *pars)

    # Euler step
    Y1 = Y + h * dY0

    # Predictor gradient
    dY1 = f(t + h, Y1, *pars)

    # corrector step
    return Y + h * (dY0 + dY1) / 2

def improved_euler_solve(f, ts, tf, Y0, h, pars=[]):
    ''' Returns time vector and respective solution at time vector
        for a derivative function using improved Euler method.

        Parameters:
        -----------
        f : callable
            Derivative function.
        ts : float
            Start time of time vector.
        tf : float
            End of time vector.
        Y0 : float
            Initial value of dependent variable.
        h : float
            Step size.
        pars : array
            Additonal parameters for derivative function, if any.

        Returns:
        --------
        t : np.array
            Time vector from ts to tf with step size h.
        Y : np.array
            Solution vector for derivative function f and time vector t.
    '''
    # create time vector
    t = np.array([])
    while ts < tf:
        t = np.append(t, ts)
        ts += h

    # solve for the value of Y at each value in the time vector
    Y = np.array([Y0])
    for i in range(len(t) - 1):
        Y = np.append(Y, improved_euler_step(f, t[i], Y[i], h, pars))
    
    return t, Y

def solve_pressure(t, ap, bp, cp, P0):
    ''' Solves pressure_ode for given time vector and parameters.
        Uses improved Euler method.

        Parameters:
        -----------
        t : array
            Time vector to solve derivative function for.
        ap : float
            Extraction rate strength parameter.
        bp : float
            Recharge strength parameter. 
        cp : float
            Slow drainage strength parameter. 
        P0 : float
            Ambient value of dependent variable.
        Pi : float
            initial value of P at t[0]

        Returns:
        --------
        Y : np.array
            Solution vector for time vector t.
    '''

    h = 0.1
    tP_solved, P_solved = improved_euler_solve(pressure_ode, 1950, 2014, P0, h, [ap, bp, cp, P0])
    return np.interp(t, tP_solved, P_solved)

def solve_temperature(t, aT, bT, Tc, T0):
    ''' Return the derivative dT/dt at time, t, for given parameters.

        Parameters:
        -----------
        t : float
            Independent variable.
        T : float
            Dependent variable.
        aT : float
            Cold water inflow strength parameter.
        bT : float
            Conduction strength parameter. 
        Tc : float
            Temperature of cold water.
        T0 : float
            Ambient value of dependent variable.
        Ti : float
            initial value of T at t[0]

        Returns:
        --------
        dTdt : float
            Derivative of dependent variable with respect to independent variable.

    '''

    h = 0.1
    tT_solved, T_solved = improved_euler_solve(temperature_ode, 1950, 2015, T0, h, [aT, bT, Tc, T0])
    return np.interp(t, tT_solved, T_solved)

# Step size
h = 0.1

### Pressure model calibration ###

# Estimates parameters (taken from previous calibration, makes it run faster and helps to avoid local minima)
P_params_estimates = [1.53674583e-01, 1.15790160e-01, 6.20234384e-01, 1.56033496e+06]
# Calibrate to obtain parameters/covariance matrix
P_params, P_covs = curve_fit(solve_pressure, tP_data, P_data, p0=P_params_estimates, sigma=[800]*len(P_data), absolute_sigma=True)
[ap, bp, cp, P0] = P_params
print('Parameters for pressure:', P_params)

# Generate model
tP_model, P_model = improved_euler_solve(pressure_ode, qt_data[0], qt_data[-1], P0, h, P_params)

# Plot pressure model against data
f, axP = plt.subplots()
axP.plot(tP_data, P_data / 1e6, 'ko', label='data')
axP.plot(tP_model, P_model / 1e6, 'b--', label='model solution')
axP.set_title('Calibrated Pressure Model vs Data')
axP.set_xlabel('time [year]')
axP.set_ylabel('pressure [MPa]')
axP.legend()
plt.show()

# Plot model misfit
f, axPm = plt.subplots()
P_misfit = np.interp(tP_data, tP_model, P_model) - P_data
axPm.plot(tP_data, P_misfit, 'bx')
axPm.set_title('Misfit of Pressure Model Against Data')
axPm.set_xlabel('time (years)')
axPm.set_ylabel('misfit (Pa)')
axPm.axhline(y=0, linestyle='dotted', color='k')
plt.show()

pressure_benchmark(save=False)
pressure_gold_secret(save=False)

### Temperature model calibration ###

# Estimates parameters (taken from previous calibration, makes it run faster and helps to avoid local minima)
T_params_guesses = [2.86738006e-06, 6.79786668e-02, 6.25359372e+01, 147]

# Set bounds so 62.5 <= Tc <= 87.5, read it somewhere in the literature that Tc ~= 75 deg C
T_bounds = ([-np.inf, -np.inf, 62.5, -np.inf], [np.inf, np.inf, 87.5, np.inf])

# Calibrates to obtain parameters/covariance
T_params, T_covs = curve_fit(solve_temperature, tT_data, T_data, p0=T_params_guesses, maxfev=20000, bounds=T_bounds, sigma=[0.08]*len(T_data), absolute_sigma=True)
[aT, bT, Tc, T0] = T_params
print('Parameters for temperature:', T_params)

# Generate model
tT_model, T_model = improved_euler_solve(temperature_ode, qt_data[0], qt_data[-1], T0, h, T_params)

# Plots temperature model against data
f, axT = plt.subplots()
axT.plot(tT_data, T_data, 'ko', label='data')
axT.plot(tT_model, T_model, 'r--', label='model solution')
axT.set_title('Calibrated Temperature Model vs Data')
axT.set_xlabel('time (year)')
axT.set_ylabel('temperature ({}C)'.format(chr(176)))
axT.legend()
plt.show()

# Plot misfit of temperature model
f, axTm = plt.subplots()
T_misfit = np.interp(tT_data, tT_model, T_model) - T_data
axTm.plot(tT_data, T_misfit, 'rx')
axTm.set_title('Misfit of Temperature Model Against Data')
axTm.set_xlabel('time (years)')
axTm.set_ylabel('misfit ({}C)'.format(chr(176)))
axTm.axhline(y=0, linestyle='dotted', color='k')
plt.show()

### Scenario Model ###

# Preserve pressure model
P_model_orig = P_model
tP_model_orig = tP_model

f, axPs = plt.subplots()

# Scenario 1
qt_data = np.concatenate([qt_data, [2014.6, 2050]])
q_data = np.concatenate([q_data, [0, 0]])
dq_data = np.concatenate([dq_data, [0, 0]])
tP_model, P_model = improved_euler_solve(pressure_ode, qt_data[0], qt_data[-1], P0, h, P_params)
axPs.plot(tP_model, P_model/1e6, 'm', label='$q_{0}$ = 0')
print('final pressure after scenario 1:')
print(P_model[-1])

tT_model_s1, T_model_s1 = improved_euler_solve(temperature_ode, qt_data[0], qt_data[-1], T0, h, T_params)
print('final temp after scenario 1:', round(T_model_s1[-1], 2))

# Scenario 2
q_data[-2:] = [500, 500]
tP_model, P_model = improved_euler_solve(pressure_ode, qt_data[0], qt_data[-1], P0, h, P_params)
axPs.plot(tP_model, P_model/1e6, 'g', label='$q_{0}$ = 10 kt/day')
print('final pressure after scenario 2:', round(P_model[-1], 2))

tT_model_s2, T_model_s2 = improved_euler_solve(temperature_ode, qt_data[0], qt_data[-1], T0, h, T_params)
print('final temp after scenario 2:', round(T_model_s2[-1], 2))

# Scenario 3
q_data[-2:] = [250, 250]
tP_model, P_model = improved_euler_solve(pressure_ode, qt_data[0], qt_data[-1], P0, h, P_params)
axPs.plot(tP_model, P_model/1e6, 'r', label='$q_{0}$ = 5 kt/day')
print('final pressure after scenario 3:', round(P_model[-1], 2))

tT_model_s3, T_model_s3 = improved_euler_solve(temperature_ode, qt_data[0], qt_data[-1], T0, h, T_params)
print('final temp after scenario 3:', round(T_model_s3[-1], 2))

# Scenario 4
q_data[-2:] = [1000, 1000]
tP_model, P_model = improved_euler_solve(pressure_ode, qt_data[0], qt_data[-1], P0, h, P_params)
axPs.plot(tP_model, P_model/1e6, 'b', label='$q_{0}$ = 1000 t/day')
print('final pressure after scenario 4:', round(P_model[-1], 2))

tT_model_s4, T_model_s4 = improved_euler_solve(temperature_ode, qt_data[0], qt_data[-1], T0, h, T_params)
print('final temp after scenario 4:', round(T_model_s4[-1], 2))

# plot data and initial model
axPs.plot(tP_data, P_data/1e6, 'bo', label='data')
axPs.plot(tP_model_orig, P_model_orig/1e6, 'k', label='model')

# Axis, labels, legend
axPs.set_title('Rotorua geothermal reservoir: what-if scenarios (pressure)')
axPs.set_xlabel('time (year)')
axPs.set_ylabel('pressure (MPa)')
axPs.legend(loc = 'lower left', fontsize = 8)
plt.show()

# Plot temperature scenario model
f, axTs = plt.subplots()
axTs.plot(tT_model_s1, T_model_s1, 'm', label='$q_{0}$ = 0')
axTs.plot(tT_model_s2, T_model_s2, 'g', label='$q_{0}$ = 500 t/day')
axTs.plot(tT_model_s3, T_model_s3, 'r', label='$q_{0}$ = 1000 t/day')
axTs.plot(tT_model_s4, T_model_s4, 'b', label='$q_{0}$ = 250 t/day')
axTs.plot(tT_data, T_data, 'ro', label='data')
axTs.plot(tT_model, T_model, 'k', label='model')

# Axis, labels, legend
axTs.set_title('Rotorua geothermal reservoir: what-if scenarios (temperature)')
axTs.set_xlabel('time (year)')
axTs.set_ylabel('temperature ($^\circ$C)')
axTs.legend(loc = 'lower left', fontsize = 8)
plt.show()


### Pressure uncertainty analysis ###

fig,axPu = plt.subplots(1,1)

ps = np.random.multivariate_normal(P_params, P_covs, 100)   # samples from posterior
for pi in ps:
    P_model = solve_pressure(tP_model_orig, *pi)
    axPu.plot(tP_model_orig, P_model/1e6, 'k-', alpha=0.2, lw=0.5)
axPu.plot([], [], 'k-', label='posterior samples')

axPu.plot(tP_data, P_data/1e6, 'bo', label='data')
axPu.set_title('Rotorua geothermal reservoir: uncertainty analysis (pressure)')
axPu.set_xlabel('time (year)')
axPu.set_ylabel('pressure (MPa)')
axPu.legend(loc = 'lower left', fontsize = 8)

plt.show()

### Temperature uncertainty analysis ###

P_model = P_model_orig
tP_model = tP_model_orig

fig,axTu = plt.subplots(1,1)

ps = np.random.multivariate_normal(T_params, T_covs, 100)   # samples from posterior
for pi in ps:
    T_model_posterior = solve_temperature(tT_model, *pi)
    axTu.plot(tT_model, T_model_posterior, 'k-', alpha=0.2, lw=0.5)
axTu.plot([], [], 'k-', label='posterior samples')

axTu.plot(tT_data, T_data, 'ro', label='data')
axTu.set_title('Rotorua geothermal reservoir: uncertainty analysis (temperature)')
axTu.set_xlabel('time (year)')
axTu.set_ylabel('temperature ($^\circ$C)')
axTu.legend(loc = 'lower left', fontsize = 8)

plt.show()

