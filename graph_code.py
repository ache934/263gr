from matplotlib import pyplot as plt
from matplotlib.legend import Legend
import numpy as np

# read data
year_water_level, water_level = np.genfromtxt('gr_p.txt',delimiter=',',skip_header=1).T
pressure_level = (101325 + 1000 * 9.81 * water_level / 2) / 1000
year_temp, temp = np.genfromtxt('gr_T.txt',delimiter=',',skip_header=1).T
year_ext_total, ext_total = np.genfromtxt('gr_q1.txt',delimiter=',',skip_header=1).T
year_ext_partial, ext_partial = np.genfromtxt('gr_q2.txt',delimiter=',',skip_header=1).T

f,ax1 = plt.subplots(nrows=1,ncols=1)

ax1.plot(year_ext_total, ext_total, 'g-', label='Total Extraction Rate')
ax1.plot(year_ext_partial, ext_partial, 'g--', label='Partial Extraction Rate\nfrom Rhyiolite Formation')
ax1.set_xlabel('time [year]')
ax1.set_ylabel('extraction rate [tonnes/day]')
ax1.set_title('Extraction Rate of Water from Rotorua Geothermal System')
ax1.legend(loc='upper left')
ax1.arrow(1989.5, 31140, -2, 0, width=200, head_length=1, color='b')
ax1.text(1990, 31140, 'Borehole closure\nprogram begins', ha='left', va ='center', size=10, color = 'b')
ax1.arrow(1967, 13500, 0, -1500, width=0.3, head_length=500, color = 'b')
ax1.text(1967, 15500, 'Waikite Geyser\nlast erupts', ha='center', va = 'top', size=10, color = 'b')

f.set_size_inches(12, 9)
if False:
    plt.savefig('fig1', bbox_inches='tight')


# figure 2: extraction rate vs water lvl and temp
f,ax2 = plt.subplots(nrows=2,ncols=1)

top_lines = []
top_lines += ax2[0].plot(year_ext_total, ext_total, 'g-')
ax2_2 = ax2[0].twinx()
top_lines += ax2_2.plot(year_water_level, pressure_level, 'b-')
ax2[0].set_ylabel('extraction rate [tonnes/day]')
ax2_2.set_ylabel('pressure [kPa]')
ax2[0].set_title("""Total Extraction Rate from Rotorua Geothermal System vs
Hydrostatic Pressure and Temperature Monitoring Data near Whakarewarewa""")
ax2[0].legend(handles=top_lines, labels=['Total Extraction Rate', 'Hydrostatic Pressure'], loc='center left')
ax2[0].arrow(1983.5, 9600, 2, 0, width=200, head_length=1, color = 'b')
ax2[0].text(1983, 9600, 'Borehole closure\nprogram begins', ha='right', va = 'center', size=10, color = 'b')

bot_lines = []
bot_lines += ax2[1].plot(year_ext_total, ext_total, 'g-', label='Total Extraction Rate')
ax2_3 = ax2[1].twinx()
bot_lines += ax2_3.plot(year_temp, temp, 'r-')
ax2[1].set_xlabel('time [year]')
ax2[1].set_ylabel('extraction rate [tonnes/day]')
ax2_3.set_ylabel('temperature [{}C]'.format(chr(176)))
ax2[1].legend(handles=bot_lines, labels=['Total Extraction Rate', 'Temperature'], loc='center left')
ax2[1].arrow(1983.5, 3500, 2, 0, width=200, head_length=1, color = 'b')
ax2[1].text(1983, 3500, 'Borehole closure\nprogram begins', ha='right', va = 'center', size=10, color = 'b')

f.set_size_inches(12, 9)
if False:
    plt.savefig('fig2', bbox_inches='tight')
plt.show()
